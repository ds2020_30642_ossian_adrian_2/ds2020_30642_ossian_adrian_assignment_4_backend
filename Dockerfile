    FROM maven:3.6.3-jdk-11 AS builder

COPY ./src/ /root/src
COPY ./pom.xml /root/
COPY ./checkstyle.xml /root/
WORKDIR /root
RUN mvn package
RUN java -Djarmode=layertools -jar /root/target/medication-platform-0.0.1-SNAPSHOT.jar list
RUN java -Djarmode=layertools -jar /root/target/medication-platform-0.0.1-SNAPSHOT.jar extract
RUN ls -l /root

FROM openjdk:11.0.6-jre

ENV TZ=UTC
ENV DB_IP=ec2-34-202-65-210.compute-1.amazonaws.com
ENV DB_PORT=5432
ENV DB_USER=rnxwuunkpsjhyy
ENV DB_PASSWORD=4a524e550f27c6834f44abb3c4c7d65d5d3659dfbda44698cf87d15871a302fc
ENV DB_DBNAME=ddsabmbppvrqv2


COPY --from=builder /root/dependencies/ ./
COPY --from=builder /root/snapshot-dependencies/ ./

RUN sleep 10
COPY --from=builder /root/spring-boot-loader/ ./
COPY --from=builder /root/application/ ./
ENTRYPOINT ["java", "org.springframework.boot.loader.JarLauncher","-XX:+UseContainerSupport -XX:+UnlockExperimentalVMOptions -XX:+UseCGroupMemoryLimitForHeap -XX:MaxRAMFraction=1 -Xms512m -Xmx512m -XX:+UseG1GC -XX:+UseSerialGC -Xss512k -XX:MaxRAM=72m"]
