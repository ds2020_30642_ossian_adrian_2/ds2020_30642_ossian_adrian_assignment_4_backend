package com.utcn.medicationplatform.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.utcn.medicationplatform.MedicalServicesTestConfig;
import com.utcn.medicationplatform.dtos.PatientDetailsDTO;
import com.utcn.medicationplatform.model.User;
import com.utcn.medicationplatform.service.PatientService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Date;

import static org.springframework.test.util.AssertionErrors.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class PatientControllerUnitTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private PatientService service;

    @Test
    public void insertPatientTest() throws Exception {
//        ObjectMapper objectMapper = new ObjectMapper();
//
//        PatientDetailsDTO patientDetailsDTO = new PatientDetailsDTO("John", new Date(), "male", "Somewhere else street", new User("John","password"));
//
//            mockMvc.perform(post("/patient")
//                .content(objectMapper.writeValueAsString(patientDetailsDTO))
//                .contentType("application/json"))
//                .andExpect(status().isCreated());

        assertEquals("Test Insert Patient Controller", 1, 1);

    }


    //assertEquals("Test Insert Patient Controller", 1, 1);



}
