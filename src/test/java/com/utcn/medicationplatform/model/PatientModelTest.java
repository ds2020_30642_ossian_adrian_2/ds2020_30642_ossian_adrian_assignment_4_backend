package com.utcn.medicationplatform.model;

import com.utcn.medicationplatform.builders.PatientBuilder;
import com.utcn.medicationplatform.dtos.PatientDTO;
import com.utcn.medicationplatform.dtos.PatientDetailsDTO;
import org.junit.Test;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PatientModelTest {

    @Test
    public void getName(){
        Patient patient = new Patient("John", new Date(), "male", "Somewhere in your heart");
        assertEquals("John", patient.getName());
    }

    @Test
    public void dtoToEntityTest() {
        PatientDetailsDTO patientDetailsDTO = new PatientDetailsDTO("John", new Date(), "male", "Somewhere else street", new User("John","password"));

        Patient thePatient = PatientBuilder.toEntity(patientDetailsDTO);
        assertEquals("John", thePatient.getName());
        assertEquals("male", thePatient.getGender());
        assertEquals("Somewhere else street", thePatient.getAdress());
        assertEquals("John", thePatient.getUser().getUsername());

    }

    @Test
    public void entityToDto(){

        Patient patient = new Patient("John", new Date(), "male", "Somewhere in your heart", new User("John","password"));
        PatientDTO patientDTO = PatientBuilder.toPatientDTO(patient);

        assertEquals("John", patientDTO.getName());
        assertEquals("male", patientDTO.getGender());
        assertEquals("Somewhere in your heart", patientDTO.getAdress());
        assertEquals("John", patientDTO.getUsername());

    }



}
