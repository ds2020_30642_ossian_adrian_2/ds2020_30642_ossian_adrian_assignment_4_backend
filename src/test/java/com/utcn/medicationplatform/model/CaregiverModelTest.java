package com.utcn.medicationplatform.model;

import org.junit.jupiter.api.Test;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CaregiverModelTest {

    @Test
    public void caregiverAgeUnder0(){

        verifyAge(null, "2022-01-01 10:10:10");

    }

    @Test
    public void caregiverAgeBetween0and18(){

        verifyAge(null, "2004-01-01 10:10:10");

    }

    @Test
    public void caregiverAgeBetween18and65(){

        try{
            DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            Date correctDate = (Date)formatter.parse("1996-01-01 10:10:10");
            verifyAge(correctDate, "1996-01-01 10:10:10");
        }
        catch(Exception e){
            System.out.println("The date is wrong!");
        }

    }

    @Test
    public void caregiverAgeOver65(){

        verifyAge(null, "1900-01-01 10:10:10");

    }

    private void verifyAge(Date expected, String theDate){

        try{
            DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            Date invalidDate = (Date)formatter.parse(theDate);
            Caregiver caregiver =  new Caregiver("John", invalidDate, "male", "Honolulu, Hawaii");
            assertEquals(caregiver.getDate(), expected);
        }
        catch(Exception e){
            System.out.println("The date is wrong!");
        }


    }

}
