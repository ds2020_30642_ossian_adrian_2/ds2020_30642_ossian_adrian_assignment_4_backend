package com.utcn.medicationplatform.service;

import com.utcn.medicationplatform.builders.MedicalRecordBuilder;
import com.utcn.medicationplatform.builders.MedicationBuilder;
import com.utcn.medicationplatform.dtos.MedicalRecordDetailsDTO;
import com.utcn.medicationplatform.dtos.MedicationDTO;
import com.utcn.medicationplatform.model.MedicalRecord;
import com.utcn.medicationplatform.model.Medication;
import com.utcn.medicationplatform.model.Patient;
import com.utcn.medicationplatform.repository.MedicalRecordRepository;
import com.utcn.medicationplatform.repository.MedicationRepository;
import com.utcn.medicationplatform.repository.PatientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class MedicalRecordService {

    public MedicalRecordRepository medicalRecordRepository;
    public PatientRepository patientRepository;
    public MedicationRepository medicationRepository;



    @Autowired

    public MedicalRecordService(MedicalRecordRepository medicalRecordRepository, PatientRepository patientRepository, MedicationRepository medicationRepository) {
        this.medicalRecordRepository = medicalRecordRepository;
        this.patientRepository = patientRepository;
        this.medicationRepository = medicationRepository;
    }

    public Optional<MedicalRecord> getMedicalRecordById(UUID id){
        return medicalRecordRepository.findById(id);
    }

    public int deleteMedicalRecord(UUID id){
        medicalRecordRepository.deleteById(id);
        return 0;
    }

    public List<MedicalRecord> getAllMedicalRecords(){
        return (List)medicalRecordRepository.findAll();
    }

    public UUID insert(MedicalRecordDetailsDTO medicalRecordDetailsDTO) {

        Optional<Patient> patient = patientRepository.findByName(medicalRecordDetailsDTO.getPatientName());
        MedicalRecord medicalRecord = MedicalRecordBuilder.toEntity(medicalRecordDetailsDTO);
        medicalRecord.setPatient(patient.get());

        for(String medicationName : medicalRecordDetailsDTO.getMedicationListAsStrings()){

            Optional<Medication> medication = medicationRepository.findByName(medicationName);
            medicalRecord.getMedicationList().add(medication.get());

        }

        medicalRecord = medicalRecordRepository.save(medicalRecord);
        patient.get().setMedicalRecord(medicalRecord);
        patientRepository.save(patient.get());
        return medicalRecord.getId();


    }

    public List<MedicationDTO> getMedication(String patientName){

        Optional<MedicalRecord> medicalRecord = medicalRecordRepository.findByPatientName(patientName);


        List<Medication> medicationList =  medicalRecord.get().getMedicationList();

        return medicationList.stream()
                .map(MedicationBuilder::toMedicationDTO)
                .collect(Collectors.toList());

    }






}
