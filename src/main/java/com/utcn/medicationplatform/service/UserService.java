package com.utcn.medicationplatform.service;

import com.utcn.medicationplatform.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.utcn.medicationplatform.repository.UserRepository;

import javax.swing.text.html.Option;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class UserService {

    public UserRepository userRepository;
    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public UUID save(User user){
        return userRepository.save(user).getId();
    }

    public User searchUser(User user){
        Optional<User> optionalUser = userRepository.findByUsername(user.getUsername());
        if(optionalUser.get() != null){
            return optionalUser.get();
        }
        return null;
}

    public Optional<User> getUserById(UUID id){
        return userRepository.findById(id);
    }

    public int deleteUser(UUID id){
        userRepository.deleteById(id);
        return 0;
    }

    public UUID updateUser(User newUser){
        return userRepository.save(newUser).getId();
    }

    public List<User> getAllUsers(){
        return (List)userRepository.findAll();
    }


}
