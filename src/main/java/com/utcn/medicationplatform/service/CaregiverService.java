package com.utcn.medicationplatform.service;

import com.utcn.medicationplatform.builders.CaregiverBuilder;
import com.utcn.medicationplatform.dtos.CaregiverDTO;
import com.utcn.medicationplatform.dtos.CaregiverDetailsDTO;
import com.utcn.medicationplatform.model.Caregiver;
import com.utcn.medicationplatform.model.User;
import com.utcn.medicationplatform.repository.CaregiverRepository;
import com.utcn.medicationplatform.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class CaregiverService {

    private static final Logger LOGGER = LoggerFactory.getLogger(PatientService.class);

    public UserRepository userRepository;

    public CaregiverRepository caregiverRepository;
    @Autowired
    public CaregiverService(CaregiverRepository caregiverRepository, UserRepository userRepository) {
        this.caregiverRepository = caregiverRepository;
        this.userRepository = userRepository;
    }

    public List<Caregiver> getAllCaregivers(){
        return (List)caregiverRepository.findAll();
    }

    public UUID updateCaregiver(CaregiverDetailsDTO caregiverDetailsDTO){

        Optional<Caregiver> caregiverOptional = caregiverRepository.findByName(caregiverDetailsDTO.getName());


        UUID userID = caregiverOptional.get().getUser().getId();
        UUID caregiverID = caregiverOptional.get().getId();


        User user = CaregiverBuilder.toEntityUser(caregiverDetailsDTO);
        user.setId(userID);
        user = userRepository.save(user);
        caregiverDetailsDTO.setUser(user);

        Caregiver caregiver = CaregiverBuilder.toEntity(caregiverDetailsDTO);
        caregiver.setId(caregiverID);
        caregiver = caregiverRepository.save(caregiver);
        LOGGER.debug("Caregiver with id {} was inserted in db", caregiver.getId());
        return caregiver.getId();

    }

    public CaregiverDTO findCaregiverById(UUID id){
        Optional<Caregiver> caregiverOptional = caregiverRepository.findById(id);
        if(!caregiverOptional.isPresent()){
            LOGGER.error("Caregiver with id {} was not found in db",id);
        }
        return CaregiverBuilder.toCaregiverDTO(caregiverOptional.get());
    }

    public List<CaregiverDTO> findCaregivers(){
        List<Caregiver> caregiverList = caregiverRepository.findAll();


        return caregiverList.stream()
                .map(CaregiverBuilder::toCaregiverDTO)
                .collect(Collectors.toList());

    }

    public UUID insert(CaregiverDetailsDTO caregiverDetailsDTO) {

        User user = CaregiverBuilder.toEntityUser(caregiverDetailsDTO);

        user = userRepository.save(user);
        UUID userId = user.getId();

        LOGGER.debug("User of type caregiver with id {} was inserted in db", userId);
        caregiverDetailsDTO.setUser(user);
        Caregiver caregiver = CaregiverBuilder.toEntity(caregiverDetailsDTO);

        caregiver = caregiverRepository.save(caregiver);
        LOGGER.debug("Caregiver with id {} was inserted in db", caregiver.getId());
        return caregiver.getId();
    }

    public int deleteByName(CaregiverDetailsDTO caregiverDetailsDTO){
        String name = caregiverDetailsDTO.getName();
        Optional<Caregiver> caregiverOptional = caregiverRepository.findByName(name);
        if(!caregiverOptional.isPresent()){
            LOGGER.error("Caregiver with name {} was not found in db", name);
        }
        UUID userID = caregiverOptional.get().getUser().getId();

        caregiverRepository.deleteById(caregiverOptional.get().getId());
        userRepository.deleteById(userID);
        return 0;
    }

    public Caregiver findCaregiverByUserId(UUID id){
        Optional<Caregiver> caregiverOptional = caregiverRepository.findByUserId(id);
        if(!caregiverOptional.isPresent()){
            return null;
        }

        return caregiverOptional.get();
    }


}
