package com.utcn.medicationplatform.service;

import com.utcn.medicationplatform.builders.MedicationBuilder;
import com.utcn.medicationplatform.dtos.MedicationDTO;
import com.utcn.medicationplatform.dtos.MedicationDetailsDTO;
import com.utcn.medicationplatform.model.Medication;
import com.utcn.medicationplatform.repository.MedicationRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class MedicationService {

    private static final Logger LOGGER = LoggerFactory.getLogger(PatientService.class);


    public MedicationRepository medicationRepository;
    @Autowired
    public MedicationService(MedicationRepository medicationRepository) {
        this.medicationRepository = medicationRepository;
    }

    public List<Medication> getAllMedication(){
        return (List)medicationRepository.findAll();
    }

    public UUID updateMedication(MedicationDetailsDTO medicationDetailsDTO){

        Optional<Medication> medicationOptional = medicationRepository.findByName(medicationDetailsDTO.getName());


        UUID medicationID = medicationOptional.get().getId();


        Medication medication = MedicationBuilder.toEntity(medicationDetailsDTO);
        medication.setId(medicationID);
        medication = medicationRepository.save(medication);
        LOGGER.debug("Medication with id {} was updated in db", medication.getId());
        return medication.getId();

    }

    public List<MedicationDTO> findMedications(){
        List<Medication> medicationList = medicationRepository.findAll();

        return medicationList.stream()
                .map(MedicationBuilder::toMedicationDTO)
                .collect(Collectors.toList());

    }

    public MedicationDTO findMedicationById(UUID id){
        Optional<Medication> medicationOptional = medicationRepository.findById(id);
        if(!medicationOptional.isPresent()){
            LOGGER.error("Medication with id {} was not found in db",id);
        }
        return MedicationBuilder.toMedicationDTO(medicationOptional.get());
    }

    public UUID insert(MedicationDetailsDTO medicationDetailsDTO) {

        Medication medication = MedicationBuilder.toEntity(medicationDetailsDTO);

        medication = medicationRepository.save(medication);
        LOGGER.debug("Medication with id {} was inserted in db", medication.getId());
        return medication.getId();
    }

    public int deleteByName(MedicationDetailsDTO medicationDetailsDTO){
        String name = medicationDetailsDTO.getName();
        Optional<Medication> medicationOptional = medicationRepository.findByName(name);
        if(!medicationOptional.isPresent()){
            LOGGER.error("Medication with name {} was not found in db", name);
            return 1;
        }

        medicationRepository.deleteById(medicationOptional.get().getId());

        return 0;
    }



}
