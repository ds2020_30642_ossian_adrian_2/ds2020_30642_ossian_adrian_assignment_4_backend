package com.utcn.medicationplatform.service;

import com.utcn.medicationplatform.builders.PatientBuilder;
import com.utcn.medicationplatform.dtos.PatientDTO;
import com.utcn.medicationplatform.dtos.PatientDetailsDTO;
import com.utcn.medicationplatform.model.Caregiver;
import com.utcn.medicationplatform.model.Patient;
import com.utcn.medicationplatform.model.User;
import com.utcn.medicationplatform.repository.CaregiverRepository;
import com.utcn.medicationplatform.repository.PatientRepository;
import com.utcn.medicationplatform.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class PatientService {

    private static final Logger LOGGER = LoggerFactory.getLogger(PatientService.class);

    public CaregiverRepository caregiverRepository;

    public UserRepository userRepository;

    public PatientRepository patientRepository;
    @Autowired
    public PatientService(PatientRepository patientRepository, UserRepository userRepository, CaregiverRepository caregiverRepository) {
        this.patientRepository = patientRepository;
        this.userRepository = userRepository;
        this.caregiverRepository = caregiverRepository;
    }

    public UUID updatePatient(PatientDetailsDTO patientDTO){

        Optional<Patient> patientOptional = patientRepository.findByName(patientDTO.getName());


        UUID userID = patientOptional.get().getUser().getId();
        UUID patientId = patientOptional.get().getId();


        User user = PatientBuilder.toEntityUser(patientDTO);
        user.setId(userID);
        user = userRepository.save(user);
        patientDTO.setUser(user);


        String caregiverName = patientDTO.getCaregiver();
        Optional<Caregiver> caregiver = caregiverRepository.findByName(caregiverName);

        Patient patient = PatientBuilder.toEntity(patientDTO);
        patient.setCaregiver(caregiver.get());
        patient.setId(patientId);
        patient = patientRepository.save(patient);
        LOGGER.debug("Patient with id {} was inserted in db", patient.getId());
        return patient.getId();

    }

    public List<Patient> getAllPatients(){
        return (List)patientRepository.findAll();
    }

    public List<PatientDTO> getAssignedPatientsWithCaregiverName(String name) {

        Optional<Caregiver> optionalCaregiver = caregiverRepository.findByName(name);

        UUID id = optionalCaregiver.get().getId();

        List<Patient> patientList = patientRepository.findByCaregiverId(id);


        return patientList.stream()
                .map(PatientBuilder::toPatientDTOForPresentingToCaregiver)
                .collect(Collectors.toList());

    }

    public List<PatientDTO> findPatients(){
        List<Patient> patientList = patientRepository.findAll();


        return patientList.stream()
                .map(PatientBuilder::toPatientDTO)
                .collect(Collectors.toList());

    }

    public PatientDTO findPatientById(UUID id){
        Optional<Patient> patientOptional = patientRepository.findById(id);
        if(!patientOptional.isPresent()){
            LOGGER.error("Patient with id {} was not found in db",id);
        }
        return PatientBuilder.toPatientDTO(patientOptional.get());
    }

    public UUID insert(PatientDetailsDTO patientDTO) {

        User user = PatientBuilder.toEntityUser(patientDTO);
        user = userRepository.save(user);
        UUID userId = user.getId();

        LOGGER.debug("User of type patient with id {} was inserted in db", userId);
        patientDTO.setUser(user);

        String caregiverName = patientDTO.getCaregiver();
        Optional<Caregiver> caregiver = caregiverRepository.findByName(caregiverName);

        Patient patient = PatientBuilder.toEntity(patientDTO);

        patient.setCaregiver(caregiver.get());
        patient = patientRepository.save(patient);
        LOGGER.debug("Patient with id {} was inserted in db", patient.getId());
        return patient.getId();
    }

    public int deleteByName(PatientDetailsDTO patientDTO){
        String name = patientDTO.getName();
        Optional<Patient> patientOptional = patientRepository.findByName(name);
        if(!patientOptional.isPresent()){
            LOGGER.error("Patient with name {} was not found in db", name);
        }
        UUID userID = patientOptional.get().getUser().getId();

        patientRepository.deleteById(patientOptional.get().getId());
        userRepository.deleteById(userID);
        return 0;
    }

    public Patient findPatientByUserId(UUID id){
        Optional<Patient> patientOptional = patientRepository.findByUserId(id);
        if(!patientOptional.isPresent()){
            return null;
        }
        return patientOptional.get();
    }

}
