package com.utcn.medicationplatform.service;

import com.utcn.medicationplatform.builders.DoctorBuilder;
import com.utcn.medicationplatform.dtos.DoctorDetailsDTO;
import com.utcn.medicationplatform.model.Doctor;
import com.utcn.medicationplatform.model.User;
import com.utcn.medicationplatform.repository.DoctorRepository;
import com.utcn.medicationplatform.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class DoctorService {

    public DoctorRepository doctorRepository;
    public UserRepository userRepository;
    @Autowired
    public DoctorService(DoctorRepository doctorRepository, UserRepository userRepository) {
        this.doctorRepository = doctorRepository;
        this.userRepository = userRepository;
    }


    public UUID insert(DoctorDetailsDTO doctorDetailsDTO) {
        User user = DoctorBuilder.toEntityUser(doctorDetailsDTO);
        user = userRepository.save(user);

        doctorDetailsDTO.setUser(user);

        Doctor doctor = DoctorBuilder.toEntity(doctorDetailsDTO);

        doctor = doctorRepository.save(doctor);

        return doctor.getId();
    }

    public Optional<Doctor> getDoctorById(UUID id){
        return doctorRepository.findById(id);
    }

    public int deleteDoctor(UUID id){
        doctorRepository.deleteById(id);
        return 0;
    }

    public UUID updateDoctor(Doctor newDoctor){
        return doctorRepository.save(newDoctor).getId();
    }

    public List<Doctor> getAllDoctors(){
        return (List)doctorRepository.findAll();
    }

    public Doctor findDoctorByUserId(UUID id){
        Optional<Doctor> doctorOptional = doctorRepository.findByUserId(id);
        if(!doctorOptional.isPresent()){
            System.out.println("Could not find doctor with id"+ id);
            return null;
        }
        return doctorOptional.get();

    }


}

