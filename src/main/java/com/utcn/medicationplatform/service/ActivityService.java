package com.utcn.medicationplatform.service;

import com.utcn.medicationplatform.builders.ActivityBuilder;
import com.utcn.medicationplatform.dtos.ActivityDetailsDTO;
import com.utcn.medicationplatform.model.Activity;
import com.utcn.medicationplatform.model.Patient;
import com.utcn.medicationplatform.repository.ActivityRepository;
import com.utcn.medicationplatform.repository.PatientRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

@Service
public class ActivityService {

    private static final Logger LOGGER = LoggerFactory.getLogger(PatientService.class);

    public ActivityRepository activityRepository;
    public PatientRepository patientRepository;

    @Autowired
    public ActivityService(ActivityRepository activityRepository, PatientRepository patientRepository) {
        this.activityRepository = activityRepository;
        this.patientRepository = patientRepository;
    }

    public UUID insert(ActivityDetailsDTO activityDetailsDTO) {


        Activity activity = ActivityBuilder.toEntity(activityDetailsDTO);

        Optional<Patient> patientOptional = patientRepository.findById(activity.getPatient_idd());


        activity.setPatient(patientOptional.get());

        activity = activityRepository.save(activity);
        activity.setPatient_idd(activityDetailsDTO.getPatient_id());
        String activityName = activity.getActivity_name();

        LOGGER.debug("Activity with name {} was inserted in db", activityName);

        return activity.getId();
    }


}
