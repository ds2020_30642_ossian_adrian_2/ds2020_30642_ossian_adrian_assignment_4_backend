package com.utcn.medicationplatform.notification;

import lombok.Getter;
import lombok.Setter;
import org.springframework.web.bind.annotation.CrossOrigin;

@CrossOrigin
@Getter
@Setter
public class AnomalyMessage {

    private String content;

    public AnomalyMessage(String content){
        this.content = content;
    }


}
