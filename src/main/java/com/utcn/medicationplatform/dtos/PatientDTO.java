package com.utcn.medicationplatform.dtos;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class PatientDTO {

    private String name;
    private String adress;
    private String gender;
    private Date birthdate;
    private String username;
    private String password;

    public PatientDTO(){
    }

    public PatientDTO(String name, String adress, String gender, Date birthdate) {
        this.name = name;
        this.adress = adress;
        this.gender = gender;
        this.birthdate = birthdate;
    }

    public PatientDTO(String name, String adress, String gender, Date birthdate, String username, String password) {
        this.name = name;
        this.adress = adress;
        this.gender = gender;
        this.birthdate = birthdate;
        this.username = username;
        this.password = password;
    }

    @Override
    public String toString() {
        return "PatientDTO{" +
                "name='" + name + '\'' +
                ", adress='" + adress + '\'' +
                ", gender='" + gender + '\'' +
                ", birthdate=" + birthdate +
                '}';
    }
}
