package com.utcn.medicationplatform.dtos;

import com.sun.istack.NotNull;
import com.utcn.medicationplatform.model.User;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.UUID;

@Getter
@Setter

public class CaregiverDetailsDTO {

    private UUID id;
    @NotNull
    private String name;

    private Date birthdate;

    private String gender;

    private String adress;

    private User user;

    private String username;

    private String password;

    private String type;

    public CaregiverDetailsDTO(){

    }

    public CaregiverDetailsDTO(String name, Date birthdate, String gender, String adress) {
        this.name = name;
        this.birthdate = birthdate;
        this.gender = gender;
        this.adress = adress;
    }

    public CaregiverDetailsDTO(UUID id, String name, Date birthdate, String gender, String adress, User user) {
        this.id = id;
        this.name = name;
        this.birthdate = birthdate;
        this.gender = gender;
        this.adress = adress;
        this.user = user;
    }


    public CaregiverDetailsDTO(String name, Date birthdate, String gender, String adress, String username, String password, String type) {
        this.name = name;
        this.birthdate = birthdate;
        this.gender = gender;
        this.adress = adress;
        this.username = username;
        this.password = password;
        this.type = type;
    }

    @Override
    public String toString() {
        return "PatientDetailsDTO{" +
                "name='" + name + '\'' +
                ", birthdate=" + birthdate +
                ", gender='" + gender + '\'' +
                ", adress='" + adress + '\'' +
                '}';
    }
}
