package com.utcn.medicationplatform.dtos;


import com.sun.istack.NotNull;
import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
public class MedicationDetailsDTO {

    private UUID id;
    @NotNull
    private String name;

    private String side_effects;

    private float dosage;

    public MedicationDetailsDTO(){

    }

    public MedicationDetailsDTO(UUID id, String name, String side_effects, float dosage) {
        this.id = id;
        this.name = name;
        this.side_effects = side_effects;
        this.dosage = dosage;
    }

    public MedicationDetailsDTO(String name, String side_effects, float dosage) {
        this.name = name;
        this.side_effects = side_effects;
        this.dosage = dosage;
    }

}
