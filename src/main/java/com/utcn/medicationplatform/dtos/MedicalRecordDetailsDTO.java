package com.utcn.medicationplatform.dtos;

import com.utcn.medicationplatform.model.Medication;
import com.utcn.medicationplatform.model.Patient;
import lombok.Getter;
import lombok.Setter;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
public class MedicalRecordDetailsDTO {

    private UUID id;

    private Date start_date;

    private Date end_date;

    private String patientName;

    private String medicalCondition;

    private int intake_interval;

    private String medicationsInOneString;

    private List<String> medicationListAsStrings;

    private Patient patientAsObject;

    private List<Medication> medicationListAsObjects;

    public MedicalRecordDetailsDTO(Date start_date, Date end_date, String patientName, String medicalCondition, int intake_interval, String medicationsInOneString) {
        this.start_date = start_date;
        this.end_date = end_date;
        this.patientName = patientName;
        this.medicalCondition = medicalCondition;
        this.intake_interval = intake_interval;
        this.medicationsInOneString = medicationsInOneString;
        this.medicationListAsStrings = Arrays.asList(medicationsInOneString.split(",", -1));
    }

    @Override
    public String toString() {
        return "MedicalRecordDetailsDTO{" +
                "start_date=" + start_date +
                ", end_date=" + end_date +
                ", patientName='" + patientName + '\'' +
                ", medicalCondition='" + medicalCondition + '\'' +
                ", intake_interval=" + intake_interval +
                ", medicationsInOneString='" + medicationsInOneString + '\'' +
                '}';
    }
}
