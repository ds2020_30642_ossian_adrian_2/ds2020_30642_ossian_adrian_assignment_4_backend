package com.utcn.medicationplatform.dtos;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MedicationDTO {

    private String name;
    private String side_effects;
    private float dosage;

    public MedicationDTO(){

    }

    public MedicationDTO(String name, String side_effects, float dosage) {
        this.name = name;
        this.side_effects = side_effects;
        this.dosage = dosage;
    }
}
