package com.utcn.medicationplatform.dtos;

import com.sun.istack.NotNull;
import com.utcn.medicationplatform.model.User;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import java.util.Date;
import java.util.UUID;

@Getter
@Setter

public class PatientDetailsDTO {

    private UUID id;
    @NotBlank
    private String name;

    @NotBlank
    private Date birthdate;

    private String gender;

    @NotBlank
    private String adress;

    @NotBlank
    private User user;

    private String username;

    private String password;

    private String type;

    private String caregiver;

    public PatientDetailsDTO(){

    }

    public PatientDetailsDTO(String name, Date birthdate, String gender, String adress) {
        this.name = name;
        this.birthdate = birthdate;
        this.gender = gender;
        this.adress = adress;
    }

    public PatientDetailsDTO(UUID id, String name, Date birthdate, String gender, String adress, User user) {
        this.id = id;
        this.name = name;
        this.birthdate = birthdate;
        this.gender = gender;
        this.adress = adress;
        this.user = user;
    }

    public PatientDetailsDTO(@NotBlank String name, @NotBlank Date birthdate, String gender, @NotBlank String adress, @NotBlank User user) {
        this.name = name;
        this.birthdate = birthdate;
        this.gender = gender;
        this.adress = adress;
        this.user = user;
    }

    public PatientDetailsDTO(String name, Date birthdate, String gender, String adress, String username, String password, String type) {
        this.name = name;
        this.birthdate = birthdate;
        this.gender = gender;
        this.adress = adress;
        this.username = username;
        this.password = password;
        this.type = type;
    }

    @Override
    public String toString() {
        return "PatientDetailsDTO{" +
                "name='" + name + '\'' +
                ", birthdate=" + birthdate +
                ", gender='" + gender + '\'' +
                ", adress='" + adress + '\'' +
                '}';
    }
}
