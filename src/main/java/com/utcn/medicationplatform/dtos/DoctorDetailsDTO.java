package com.utcn.medicationplatform.dtos;

import com.sun.istack.NotNull;
import com.utcn.medicationplatform.model.User;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.UUID;

@Getter
@Setter

public class DoctorDetailsDTO {

    private UUID id;
    @NotNull
    private String name;

    private Date birthdate;

    private String adress;

    private User user;

    private String username;

    private String password;

    private String type;


    public DoctorDetailsDTO() {
    }

    public DoctorDetailsDTO(UUID id, String name, Date birthdate, String adress, User user) {
        this.id = id;
        this.name = name;
        this.birthdate = birthdate;
        this.adress = adress;
        this.user = user;
    }

    public DoctorDetailsDTO(String name, Date birthdate, String adress) {
        this.name = name;
        this.birthdate = birthdate;
        this.adress = adress;
    }
}
