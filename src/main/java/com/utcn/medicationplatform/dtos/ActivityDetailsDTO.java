package com.utcn.medicationplatform.dtos;

import com.utcn.medicationplatform.model.Patient;
import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
public class ActivityDetailsDTO {


    private UUID id;

    private Patient patient;

    private UUID patient_id;

    private String activity;

    private Long start;

    private Long end;

    public ActivityDetailsDTO(UUID patient_id, String activity, Long start, Long end) {
        this.patient_id = patient_id;
        this.activity = activity;
        this.start = start;
        this.end = end;
    }

    public ActivityDetailsDTO(Patient patient, String activity, Long start, Long end) {
        this.patient = patient;
        this.activity = activity;
        this.start = start;
        this.end = end;
    }

    public ActivityDetailsDTO(Patient patient, UUID patient_id, String activity, Long start, Long end) {
        this.patient = patient;
        this.patient_id = patient_id;
        this.activity = activity;
        this.start = start;
        this.end = end;
    }

    public ActivityDetailsDTO(UUID id, Patient patient, UUID patient_id, String activity, Long start, Long end) {
        this.id = id;
        this.patient = patient;
        this.patient_id = patient_id;
        this.activity = activity;
        this.start = start;
        this.end = end;
    }

    public ActivityDetailsDTO() {
    }
}
