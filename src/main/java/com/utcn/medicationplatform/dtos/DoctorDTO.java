package com.utcn.medicationplatform.dtos;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class DoctorDTO {


    private String name;
    private String adress;
    private Date birthdate;
    private String username;
    private String password;

    public DoctorDTO() {
    }

    public DoctorDTO(String name, String adress, Date birthdate) {
        this.name = name;
        this.adress = adress;
        this.birthdate = birthdate;
    }

    public DoctorDTO(String name, String adress, Date birthdate, String username, String password) {
        this.name = name;
        this.adress = adress;
        this.birthdate = birthdate;
        this.username = username;
        this.password = password;
    }
}
