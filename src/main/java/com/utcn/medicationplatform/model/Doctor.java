package com.utcn.medicationplatform.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@Table(name = "\"doctor\"")
@Entity
@Getter
@Setter
@NoArgsConstructor
public class Doctor {

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Type(type = "uuid-binary")
    private UUID id;

    @Column(name = "name")
    private String name;

    @Column(name = "birthdate")
    private Date date;

    @Column(name = "adress")
    private String adress;

    @JoinColumn(name="user_id")
    @OneToOne
    private User user;

    public Doctor(String name, Date date, String adress, User user) {
        this.name = name;
        this.date = date;
        this.adress = adress;
        this.user = user;
    }

    @Override
    public String toString() {
        return "Doctor{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", date=" + date +
                ", adress='" + adress + '\'' +
                '}';
    }
}
