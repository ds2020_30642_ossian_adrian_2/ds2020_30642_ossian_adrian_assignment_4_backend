package com.utcn.medicationplatform.model;


import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

@Table(name = "\"medication\"")
@Entity
@Getter
@Setter
@NoArgsConstructor


public class Medication {


    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Type(type = "uuid-binary")
    private UUID id;

    @Column(name = "name")
    private String name;

    @Column(name = "side_effects")
    private String side_effects;

    @Column(name = "dosage")
    private float dosage;

    @ManyToMany(mappedBy = "medicationList", fetch = FetchType.EAGER,cascade=CascadeType.MERGE)
    List<MedicalRecord> medicalRecords;


    public Medication(String name, String side_effects, float dosage) {
        this.name = name;
        this.side_effects = side_effects;
        this.dosage = dosage;
    }

    @Override
    public String toString() {
        return "Medication{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", side_effects='" + side_effects + '\'' +
                ", dosage=" + dosage +
                '}';
    }
}
