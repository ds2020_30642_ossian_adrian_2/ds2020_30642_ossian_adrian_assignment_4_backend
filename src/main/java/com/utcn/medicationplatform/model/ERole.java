package com.utcn.medicationplatform.model;

public enum ERole {
    ROLE_PATIENT,
    ROLE_CAREGIVER,
    ROLE_ADMIN
}