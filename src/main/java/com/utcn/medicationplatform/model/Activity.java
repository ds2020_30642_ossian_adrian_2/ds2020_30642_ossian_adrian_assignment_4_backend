package com.utcn.medicationplatform.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.util.UUID;

@Table(name = "\"activitypatient\"")
@Entity
@Getter
@Setter
@NoArgsConstructor

public class Activity implements Serializable {

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Type(type = "uuid-binary")
    private UUID id;


    @JsonBackReference(value="user-movement2")
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "patient_id")
    private Patient patient;
//
    @Column(name = "patient_idd")
    private UUID patient_idd;

    @Column(name = "activity_name")
    private String activity_name;

    @Column(name = "start_time")
    private Long start_time;

    @Column(name = "end_time")
    private Long end_time;

    public Activity(UUID patient_id, String activity, Long start, Long end) {
        this.patient_idd = patient_id;
        this.activity_name = activity;
        this.start_time = start;
        this.end_time = end;
    }

    public Activity(Patient patient, String activity, Long start, Long end) {
        this.patient = patient;
        this.patient_idd = patient.getId();
        this.activity_name = activity;
        this.start_time = start;
        this.end_time = end;
    }

    @Override
    public String toString() {
        return "Activity{" +
                //"patient_id=" + patient_idd +
                ", activity='" + activity_name + '\'' +
                ", start=" + start_time +
                ", end=" + end_time +
                '}';
    }
}
