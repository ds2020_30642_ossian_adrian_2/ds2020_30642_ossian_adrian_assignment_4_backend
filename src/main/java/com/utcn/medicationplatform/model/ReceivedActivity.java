package com.utcn.medicationplatform.model;


import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.UUID;

@Getter
@Setter
public class ReceivedActivity implements Serializable {

    private UUID patient_id;
    private String activity;
    private Long start;
    private Long end;

    public ReceivedActivity(UUID patient_id, String activity, Long start, Long end) {
        this.patient_id = patient_id;
        this.activity = activity;
        this.start = start;
        this.end = end;
    }

    @Override
    public String toString() {
        return "Activity{" +
                "patient_id=" + patient_id +
                ", activity='" + activity + '\'' +
                ", start=" + start +
                ", end=" + end +
                '}';
    }
}