package com.utcn.medicationplatform.model;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.UUID;


@Getter
@Setter
public class TypeOfUser {

    private UUID id;

    private String type;

    private String name;

    private Date birthdate;

    private String gender;

    private String adress;

    public TypeOfUser() {
    }

    public TypeOfUser(UUID id, String type, String name, Date birthdate, String gender, String adress) {
        this.id = id;
        this.type = type;
        this.name = name;
        this.birthdate = birthdate;
        this.gender = gender;
        this.adress = adress;
    }

    public TypeOfUser(UUID id, String type, String name) {
        this.id = id;
        this.type = type;
        this.name = name;
    }

}
