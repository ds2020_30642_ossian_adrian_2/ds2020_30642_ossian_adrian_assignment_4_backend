package com.utcn.medicationplatform.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Table(name = "\"medicalrecord\"")
@Entity
@Getter
@Setter
@NoArgsConstructor

public class MedicalRecord {

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Type(type = "uuid-binary")
    private UUID id;

    @Column(name = "start_date")
    private Date start_date;

    @Column(name = "end_date")
    private Date end_date;

    @JoinColumn(name="patient_id")
    @OneToOne
    private Patient patient;

    @Column(name = "medical_condition")
    private String medical_condition;

    @Column(name = "intake_interval")
    private int intake_interval;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
    @JoinTable(
            name = "medicalrecord_medication",
            joinColumns = @JoinColumn(name = "medicalrecord_id"),
            inverseJoinColumns = @JoinColumn(name = "medication_id"))
    private List<Medication> medicationList;

    @Override
    public String toString() {
        return "MedicalRecord{" +
                "id=" + id +
                ", start_date=" + start_date +
                ", end_date='" + end_date + '\'' +
                ", medical_condition='" + medical_condition + '\'' +
                ", intake_interval=" + intake_interval +
                '}';
    }

    public MedicalRecord(Date start_date, Date end_date, String medical_condition, int intake_interval) {
        this.start_date = start_date;
        this.end_date = end_date;
        this.medical_condition = medical_condition;
        this.intake_interval = intake_interval;
        this.medicationList = new ArrayList<>();
    }
}
