package com.utcn.medicationplatform.model;


import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Table(name = "\"patient\"")
@Entity
@Getter
@Setter
@NoArgsConstructor

public class Patient {


    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Type(type = "uuid-binary")
    private UUID id;

    @NotBlank
    @Column(name = "name")
    private String name;

    @NotBlank
    @Column(name = "birthdate")
    private Date birthdate;

    @Column(name = "gender")
    private String gender;

    @NotBlank
    @Column(name = "adress")
    private String adress;

    @NotBlank
    @JoinColumn(name="user_id")
    @OneToOne
    private User user;

    @JsonBackReference(value="user-movement")
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "caregiver_id")
    private Caregiver caregiver;

    @JsonBackReference
    @JoinColumn(name="medicalrecord_id")
    @OneToOne
    private MedicalRecord medicalRecord;

    @JsonManagedReference(value="user-movement2")
    @OneToMany(mappedBy = "patient", fetch = FetchType.LAZY)
    private List<Activity> activities;

    @Override
    public String toString() {
        return "Patient{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", date=" + birthdate +
                ", gender='" + gender + '\'' +
                ", adress='" + adress + '\'' +
                '}';
    }

    public Patient(String name, Date birthdate, String gender, String adress) {
        this.name = name;
        this.birthdate = birthdate;
        this.gender = gender;
        this.adress = adress;
    }

    public Patient(String name, Date birthdate, String gender, String adress, User user) {
        this.name = name;
        this.birthdate = birthdate;
        this.gender = gender;
        this.adress = adress;
        this.user = user;
    }


}


