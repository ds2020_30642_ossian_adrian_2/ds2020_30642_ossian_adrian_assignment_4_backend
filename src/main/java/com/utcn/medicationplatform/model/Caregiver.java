package com.utcn.medicationplatform.model;


import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Table(name = "\"caregiver\"")
@Entity
@Getter
@Setter
@NoArgsConstructor
public class Caregiver {

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Type(type = "uuid-binary")
    private UUID id;

    @Column(name = "name")
    private String name;

    @Column(name = "birthdate")
    private Date date;

    @Column(name = "gender")
    private String gender;

    @Column(name = "adress")
    private String adress;

    @JoinColumn(name="user_id")
    @OneToOne
    private User user;

    @JsonManagedReference(value="user-movement")
    @OneToMany(mappedBy = "caregiver", fetch = FetchType.LAZY)
    private List<Patient> patients;



    @Override
    public String toString() {
        return "Caregiver{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", date=" + date +
                ", gender='" + gender + '\'' +
                ", adress='" + adress + '\'' +
                '}';
    }

    public Caregiver(String name, Date date, String gender, String adress, User user) {
        this.name = name;
        this.date = date;
        this.gender = gender;
        this.adress = adress;
        this.user = user;
    }

    public Caregiver(String name, Date date, String gender, String adress) {
        this.name = name;
        //get current date
        Date currentDate = new Date();
        Long currentTime = currentDate.getTime();
        Long givenTime = date.getTime();
        Long ageInMilliseconds = currentTime - givenTime;
        //if age is lower than 18 years or higher than 65 years
        if(ageInMilliseconds >  2051201900000L || ageInMilliseconds < 568024668000L){
            this.date = null;
        }
        else{
            this.date = date;
        }
        this.gender = gender;
        this.adress = adress;
    }


}
