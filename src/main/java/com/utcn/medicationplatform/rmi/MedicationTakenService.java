package com.utcn.medicationplatform.rmi;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

public interface MedicationTakenService extends Remote {

    public List<MedicationDTO2> getMedicationForPatient(String name) throws RemoteException;

    public List<MedicationDTO2> takeOneMedication(String name) throws RemoteException;

    public String wasCorrect(String name) throws RemoteException;

}
