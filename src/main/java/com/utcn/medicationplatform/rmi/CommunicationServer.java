package com.utcn.medicationplatform.rmi;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
public class CommunicationServer extends UnicastRemoteObject implements MedicationTakenService {

    private static List<MedicationDTO2> medicationForPatient;
    private static String wasCorrectlyTaken;

    public CommunicationServer() throws RemoteException{
        super();

        medicationForPatient = new ArrayList<>();
//
//        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
//        List<String> datesAsStrings = new ArrayList<>();
//        datesAsStrings.add("2020-12-03 10:34:23");
//        datesAsStrings.add("2020-12-03 14:34:23");
//        datesAsStrings.add("2019-12-04 17:34:23");
//        datesAsStrings.add("2020-12-05 20:34:23");
//
//        List<String> medicationNames = new ArrayList<>();
//        medicationNames.add("paracetamol");
//        medicationNames.add("furazolidon");
//        medicationNames.add("vitamina c");
//        medicationNames.add("augmentin");
//
//
//
//        try {
//
//            int i = 0;
//            for(String dateAsString : datesAsStrings){
//
//
//                Date theDate = (Date)formatter.parse(dateAsString);
//                Long millisForDate = theDate.getTime();
//                MedicationDTO2 medication = new MedicationDTO2(medicationNames.get(i), theDate, millisForDate);
//
//                medicationForPatient.add(medication);
//                i++;
//
//            }
//
//
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//
//

    }

    @Override
    public List<MedicationDTO2> getMedicationForPatient(String name) throws RemoteException {

        medicationForPatient = new ArrayList<>();

        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        List<String> datesAsStrings = new ArrayList<>();
        datesAsStrings.add("2020-12-03 10:34:23");
        datesAsStrings.add("2020-12-03 14:34:23");
        datesAsStrings.add("2019-12-04 17:34:23");
        datesAsStrings.add("2020-12-05 20:34:23");

        List<String> medicationNames = new ArrayList<>();
        medicationNames.add("paracetamol");
        medicationNames.add("furazolidon");
        medicationNames.add("vitamina c");
        medicationNames.add("augmentin");



        try {

            int i = 0;
            for(String dateAsString : datesAsStrings){


                Date theDate = (Date)formatter.parse(dateAsString);
                Long millisForDate = theDate.getTime();
                MedicationDTO2 medication = new MedicationDTO2(medicationNames.get(i), theDate, millisForDate);

                medicationForPatient.add(medication);
                i++;

            }


        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }


        return medicationForPatient;
    }

    @Override
    public List<MedicationDTO2> takeOneMedication(String name) throws RemoteException {

        MedicationDTO2 takenMedication = medicationForPatient.remove(0);

        Date date = new Date();
        //This method returns the time in millis
        Long currentMillis = date.getTime();

        //allow 10 minutes for the patient to take the medication
        if(takenMedication.getTime() + 600000 > currentMillis){ // && takenMedication.getTime() > currentMillis
            System.out.println("[Medication server] Medication " + takenMedication.getName() + " was taken within the allocated 10 minutes. ");
            wasCorrectlyTaken = "Yes";
        }
        else{
            wasCorrectlyTaken = "No";
            System.out.println("[Medication server] Medication " + takenMedication.getName() + " wasn't taken within the allocated 10 minutes. The patient is in great trouble. RIP.");
        }

        medicationForPatient.remove(takenMedication);
        return medicationForPatient;
    }

    @Override
    public String wasCorrect(String name) throws RemoteException {

        if(wasCorrectlyTaken == null){
            return "Yes";
        }
        else {
            return wasCorrectlyTaken;
        }

    }


}