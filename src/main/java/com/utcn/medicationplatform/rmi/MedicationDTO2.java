package com.utcn.medicationplatform.rmi;

import java.io.Serializable;
import java.util.Date;

public class MedicationDTO2 implements Serializable {


    private String name;
    private Date date;
    private Long time;

    public MedicationDTO2(String name, Date date, Long time) {
        this.name = name;
        this.date = date;
        this.time = time;
    }

    public MedicationDTO2(String name, Long time) {
        this.name = name;
        this.time = time;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
