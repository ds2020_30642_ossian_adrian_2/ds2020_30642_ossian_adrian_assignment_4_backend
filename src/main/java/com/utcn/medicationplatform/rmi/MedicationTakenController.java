package com.utcn.medicationplatform.rmi;


import com.utcn.medicationplatform.rmi.MedicationDTO2;
import com.utcn.medicationplatform.rmi.MedicationTakenService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping(value = "/medicationtaken")
public class MedicationTakenController {

    public CommunicationServer communicationServer;

    public MedicationTakenController() throws RemoteException {
        this.communicationServer = new CommunicationServer();
    }

    @GetMapping()
    public ResponseEntity<List<MedicationDTO2>> getMedicationToTake() throws RemoteException, NotBoundException, MalformedURLException {


        MedicationTakenService service = (MedicationTakenService) Naming.lookup("rmi://localhost:5089/medication");
        List<MedicationDTO2> medications = service.getMedicationForPatient("Patient_adi");

//        List<MedicationDTO2> medications = communicationServer.getMedicationForPatient("Patient_adi");

        return new ResponseEntity<>(medications, HttpStatus.OK);
    }

    @GetMapping(path = "/takeone")
    public ResponseEntity<List<MedicationDTO2>> takeOneMedication() throws RemoteException, NotBoundException, MalformedURLException {


        MedicationTakenService service = (MedicationTakenService) Naming.lookup("rmi://localhost:5089/medication");
        List<MedicationDTO2> medications = service.takeOneMedication("Patient_adi");


//        List<MedicationDTO2> medications = communicationServer.takeOneMedication("Patient_adi");


        return new ResponseEntity<>(medications, HttpStatus.OK);
    }

    @GetMapping(path = "/wascorrect")
    public ResponseEntity<List<MedicationDTO2>> wasCorrect() throws RemoteException, NotBoundException, MalformedURLException {


        MedicationTakenService service = (MedicationTakenService) Naming.lookup("rmi://localhost:5089/medication");
        String ans = service.wasCorrect("Patient_adi");


//        String ans = communicationServer.wasCorrect("Patient_adi");

        List<MedicationDTO2> medications = new ArrayList<>();
        if(ans.equals("Yes")){
            return new ResponseEntity<>(medications, HttpStatus.OK);
        }
        else{
            return new ResponseEntity<>(medications, HttpStatus.GONE);
        }

       }



}
