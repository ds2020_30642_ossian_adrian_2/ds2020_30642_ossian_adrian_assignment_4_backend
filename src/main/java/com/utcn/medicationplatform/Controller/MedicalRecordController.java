package com.utcn.medicationplatform.Controller;

import com.utcn.medicationplatform.dtos.MedicalRecordDetailsDTO;
import com.utcn.medicationplatform.model.MedicalRecord;
import com.utcn.medicationplatform.service.MedicalRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@CrossOrigin
@RestController
@RequestMapping(value = "/medicalrecord")
public class MedicalRecordController {

    private MedicalRecordService medicalRecordService;

    @Autowired
    public MedicalRecordController(MedicalRecordService medicalRecordService){
        this.medicalRecordService = medicalRecordService;
    }

    @PostMapping()
    public ResponseEntity<UUID> insertMedicalRecord(@RequestBody MedicalRecordDetailsDTO medicalRecordDetailsDTO){
        UUID medicalRecordID = medicalRecordService.insert(medicalRecordDetailsDTO);
        return new ResponseEntity<>(medicalRecordID, HttpStatus.CREATED);
    }

    @GetMapping(path = "/{id}")
    public MedicalRecord getMedicalRecordById(@PathVariable("id") UUID id){
        return medicalRecordService.getMedicalRecordById(id)
                .orElse(null);
    }

    @DeleteMapping(path = "/{id}")
    public void deleteMedicalRecordById(@PathVariable("id") UUID id){
        medicalRecordService.deleteMedicalRecord(id);
    }


    @GetMapping(path = "/all")
    public List<MedicalRecord> getAllMedicalRecords(){
        return medicalRecordService.getAllMedicalRecords();
    }

}
