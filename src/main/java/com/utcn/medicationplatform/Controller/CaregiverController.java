package com.utcn.medicationplatform.Controller;


import com.utcn.medicationplatform.dtos.CaregiverDTO;
import com.utcn.medicationplatform.dtos.CaregiverDetailsDTO;
import com.utcn.medicationplatform.model.Caregiver;
import com.utcn.medicationplatform.service.CaregiverService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@CrossOrigin
@RestController
@RequestMapping(value = "/caregiver")
public class CaregiverController {

    private CaregiverService caregiverService;
    @Autowired
    public CaregiverController(CaregiverService caregiverService){ this.caregiverService = caregiverService; }

    @GetMapping(path = "/all")
    public List<Caregiver> getAllCaregivers(){
        return caregiverService.getAllCaregivers();
    }

    @GetMapping()
    public ResponseEntity<List<CaregiverDTO>> getCaregivers(){
        List<CaregiverDTO> dtos = caregiverService.findCaregivers();
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity<UUID> insertCaregiver(@RequestBody CaregiverDetailsDTO caregiverDetailsDTO){
        UUID caregiverId = caregiverService.insert(caregiverDetailsDTO);
        return new ResponseEntity<>(caregiverId, HttpStatus.CREATED);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<CaregiverDTO> getCaregiver(@PathVariable("id") UUID caregiverId) {
        CaregiverDTO dto = caregiverService.findCaregiverById(caregiverId);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @DeleteMapping()
    public ResponseEntity<Integer> deleteCaregiver(@RequestBody CaregiverDetailsDTO caregiverDetailsDTO) {
        Integer res = caregiverService.deleteByName(caregiverDetailsDTO);
        return new ResponseEntity<>(res, HttpStatus.OK);
    }

    @PutMapping()
    public ResponseEntity<UUID> updateCaregiver(@RequestBody CaregiverDetailsDTO caregiverDetailsDTO){
        UUID caregiverId = caregiverService.updateCaregiver(caregiverDetailsDTO);
        return new ResponseEntity<>(caregiverId, HttpStatus.OK);
    }


}
