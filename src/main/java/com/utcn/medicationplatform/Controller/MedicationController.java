package com.utcn.medicationplatform.Controller;

import com.utcn.medicationplatform.dtos.MedicationDTO;
import com.utcn.medicationplatform.dtos.MedicationDetailsDTO;
import com.utcn.medicationplatform.dtos.PatientDTO;
import com.utcn.medicationplatform.dtos.PatientDetailsDTO;
import com.utcn.medicationplatform.model.Medication;
import com.utcn.medicationplatform.service.MedicationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@CrossOrigin
@RestController
@RequestMapping(value = "/medication")
public class MedicationController {

    private MedicationService medicationService;

    @Autowired
    public MedicationController(MedicationService medicationService){ this.medicationService = medicationService; }

    @GetMapping(path = "/all")
    public List<Medication> getAllMedication(){
        return medicationService.getAllMedication();
    }

    @GetMapping()
    public ResponseEntity<List<MedicationDTO>> getMedications(){
        List<MedicationDTO> dtos = medicationService.findMedications();
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity<UUID> insertMedication(@RequestBody MedicationDetailsDTO medicationDetailsDTO){
        UUID medicationID = medicationService.insert(medicationDetailsDTO);
        return new ResponseEntity<>(medicationID, HttpStatus.CREATED);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<MedicationDTO> getMedication(@PathVariable("id") UUID medicationID) {
        MedicationDTO dto = medicationService.findMedicationById(medicationID);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @DeleteMapping()
    public ResponseEntity<Integer> deleteMedication(@RequestBody MedicationDetailsDTO medicationDetailsDTO) {
        Integer res = medicationService.deleteByName(medicationDetailsDTO);
        return new ResponseEntity<>(res, HttpStatus.OK);
    }

    @PutMapping()
    public ResponseEntity<UUID> updateMedication(@RequestBody MedicationDetailsDTO medicationDetailsDTO){
        UUID medicationID = medicationService.updateMedication(medicationDetailsDTO);
        return new ResponseEntity<>(medicationID, HttpStatus.OK);
    }


}
