package com.utcn.medicationplatform.Controller;

import com.utcn.medicationplatform.dtos.MedicationDTO;
import com.utcn.medicationplatform.dtos.PatientDTO;
import com.utcn.medicationplatform.dtos.PatientDetailsDTO;
import com.utcn.medicationplatform.model.Doctor;
import com.utcn.medicationplatform.model.Patient;
import com.utcn.medicationplatform.service.MedicalRecordService;
import com.utcn.medicationplatform.service.PatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.UUID;

@CrossOrigin
@RestController
@RequestMapping(value = "/patient")
public class PatientController {


    private PatientService patientService;
    private MedicalRecordService medicalRecordService;

    @Autowired
    public PatientController(PatientService patientService, MedicalRecordService medicalRecordService) {
        this.patientService = patientService;
        this.medicalRecordService = medicalRecordService;
    }

    @GetMapping(path = "/all")
    public List<Patient> getAllUsers(){
        return patientService.getAllPatients();
    }

    @GetMapping(path = "/caregiver/{name}")
    public ResponseEntity<List<PatientDTO>> getAllPatientsWithCaregiverName(@PathVariable("name") String name){
        List<PatientDTO> dtos = patientService.getAssignedPatientsWithCaregiverName(name);
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @GetMapping()
    public ResponseEntity<List<PatientDTO>> getPatients(){
        List<PatientDTO> dtos = patientService.findPatients();
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity<UUID> insertPatient(@Valid @NotNull @RequestBody PatientDetailsDTO patientDTO){
        UUID patientID = patientService.insert(patientDTO);
        return new ResponseEntity<>(patientID, HttpStatus.CREATED);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<PatientDTO> getPatient(@PathVariable("id") UUID patientID) {
        PatientDTO dto = patientService.findPatientById(patientID);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @DeleteMapping()
    public ResponseEntity<Integer> deletePatient(@RequestBody PatientDetailsDTO patientDTO) {
        Integer res = patientService.deleteByName(patientDTO);
        return new ResponseEntity<>(res, HttpStatus.OK);
    }

    @PutMapping()
    public ResponseEntity<UUID> updatePatient(@RequestBody PatientDetailsDTO patientDTO){
        UUID patientId = patientService.updatePatient(patientDTO);
        return new ResponseEntity<>(patientId, HttpStatus.OK);
    }

    @GetMapping(value = "/medication/{patientName}")
    public ResponseEntity<List<MedicationDTO>> getMedication(@PathVariable("patientName") String patientName) {
        List<MedicationDTO> dtos = medicalRecordService.getMedication(patientName);
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }


}
