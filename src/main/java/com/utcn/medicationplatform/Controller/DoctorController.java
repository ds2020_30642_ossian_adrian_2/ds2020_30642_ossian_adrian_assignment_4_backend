package com.utcn.medicationplatform.Controller;


import com.utcn.medicationplatform.dtos.DoctorDetailsDTO;
import com.utcn.medicationplatform.model.Doctor;
import com.utcn.medicationplatform.service.DoctorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@CrossOrigin
@RestController
@RequestMapping(value = "/doctor")
public class DoctorController {

    private DoctorService doctorService;
    @Autowired
    public DoctorController(DoctorService doctorService){ this.doctorService = doctorService; }

    @PostMapping()
    public ResponseEntity<UUID> insertDoctor(@RequestBody DoctorDetailsDTO doctorDTO){
        UUID doctorID = doctorService.insert(doctorDTO);
        return new ResponseEntity<>(doctorID, HttpStatus.CREATED);
    }

    @GetMapping(path = "/{id}")
    public Doctor getDoctorById(@PathVariable("id") UUID id){
        return doctorService.getDoctorById(id)
                .orElse(null);
    }

    @DeleteMapping(path = "/{id}")
    public void deleteDoctorById(@PathVariable("id") UUID id){
        doctorService.deleteDoctor(id);
    }

    @PutMapping()
    public void updateDoctor(@RequestBody Doctor doctor){
        doctorService.updateDoctor(doctor);
    }

    @GetMapping(path = "/all")
    public List<Doctor> getAllUsers(){
        return doctorService.getAllDoctors();
    }

}
