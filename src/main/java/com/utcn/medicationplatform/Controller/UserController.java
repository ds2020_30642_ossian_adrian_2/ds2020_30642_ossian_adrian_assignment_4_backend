package com.utcn.medicationplatform.Controller;

import com.utcn.medicationplatform.model.*;
import com.utcn.medicationplatform.service.CaregiverService;
import com.utcn.medicationplatform.service.DoctorService;
import com.utcn.medicationplatform.service.PatientService;
import com.utcn.medicationplatform.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.UUID;

@CrossOrigin
@RestController
@RequestMapping(value = "/user")
public class UserController {

    private TypeOfUser currentUser = new TypeOfUser();

    private UserService userService;
    private CaregiverService caregiverService;
    private PatientService patientService;
    private DoctorService doctorService;


    @Autowired
    public UserController(UserService userService, CaregiverService caregiverService, PatientService patientService, DoctorService doctorService) {
        this.userService = userService;
        this.caregiverService = caregiverService;
        this.patientService = patientService;
        this.doctorService = doctorService;
    }

    @PostMapping()
    public ResponseEntity<UUID> create(@RequestBody User user){
        UUID userID = userService.save(user);
        return new ResponseEntity<>(userID, HttpStatus.CREATED);
    }

    @GetMapping(path = "/{id}")
    public User getUserById(@PathVariable("id") UUID id){
        return userService.getUserById(id)
                .orElse(null);
    }

    @GetMapping(path = "/current")
    public ResponseEntity<TypeOfUser> getCurrentUser(){
        return new ResponseEntity<>(currentUser, HttpStatus.OK);
    }

    @GetMapping(path = "/logout")
    public ResponseEntity<Integer> logOutUser(){
        Integer integer = 1;
        currentUser = new TypeOfUser();
        return new ResponseEntity<>(integer, HttpStatus.OK);
    }

    @GetMapping(path = "/login/{username}/{password}")
    public ResponseEntity<TypeOfUser> loginUser(@PathVariable("username") String username, @PathVariable("password") String password){

        User user = new User(username, password);
        User userReturned = userService.searchUser(user);

        if(user.getUsername().equals(userReturned.getUsername()) && user.getPassword().equals(userReturned.getPassword())){

            Caregiver caregiver = verifyIfCaregiver(userReturned.getId());

            if  (caregiver != null ){
                currentUser = new TypeOfUser(caregiver.getId(), "caregiver", caregiver.getName(), caregiver.getDate(), caregiver.getGender(),caregiver.getAdress());
                return new ResponseEntity<>( new TypeOfUser(caregiver.getId(), "caregiver", caregiver.getName(), caregiver.getDate(), caregiver.getGender(),caregiver.getAdress()), HttpStatus.OK);
            }
            Patient patient = verifyIfPatient(userReturned.getId());
            if  (patient != null ){
                currentUser = new TypeOfUser(patient.getId(), "patient", patient.getName(), patient.getBirthdate(), patient.getGender(),patient.getAdress());
                return new ResponseEntity<>( new TypeOfUser(patient.getId(), "patient", patient.getName(), patient.getBirthdate(), patient.getGender(),patient.getAdress()), HttpStatus.OK);
            }

            Doctor doctor = verifyIfDoctor(userReturned.getId());
            if  (doctor != null ){
                currentUser = new TypeOfUser(doctor.getId(), "doctor", doctor.getName(), new Date(), "male", "Address");
                return new ResponseEntity<>( new TypeOfUser(doctor.getId(), "doctor", doctor.getName(), new Date(), "male", "Address"), HttpStatus.OK);
            }


            return new ResponseEntity<>(new TypeOfUser(), HttpStatus.NOT_ACCEPTABLE);
        }


        return new ResponseEntity<>(new TypeOfUser(), HttpStatus.NOT_ACCEPTABLE);


    }

    @DeleteMapping(path = "/{id}")
    public void deleteUserById(@PathVariable("id") UUID id){
        userService.deleteUser(id);
    }

    @PutMapping()
    public void updateUser(@RequestBody User user){
        userService.updateUser(user);
    }

    @GetMapping(path = "/all")
    public List<User> getAllUsers(){
        return userService.getAllUsers();
    }

    public Caregiver verifyIfCaregiver(UUID id){

        Caregiver caregiver = caregiverService.findCaregiverByUserId(id);
        if(caregiver != null){
            return caregiver;
        }
        return null;
    }

    public Patient verifyIfPatient(UUID id){

        Patient patient = patientService.findPatientByUserId(id);
        if(patient != null){
            return patient;
        }
        return null;
    }

    public Doctor verifyIfDoctor(UUID id){

        Doctor doctor = doctorService.findDoctorByUserId(id);
        if(doctor != null){
            return doctor;
        }
        return null;
    }



}
