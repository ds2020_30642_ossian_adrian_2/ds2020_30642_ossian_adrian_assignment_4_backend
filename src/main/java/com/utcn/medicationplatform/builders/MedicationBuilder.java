package com.utcn.medicationplatform.builders;

import com.utcn.medicationplatform.dtos.MedicationDTO;
import com.utcn.medicationplatform.dtos.MedicationDetailsDTO;
import com.utcn.medicationplatform.model.Medication;

public class MedicationBuilder {

    private MedicationBuilder(){

    }

    public static MedicationDTO toMedicationDTO(Medication medication){

        return new MedicationDTO(medication.getName(), medication.getSide_effects(), medication.getDosage());
    }

    public static Medication toEntity(MedicationDetailsDTO medicationDetailsDTO){

        return new Medication(medicationDetailsDTO.getName(), medicationDetailsDTO.getSide_effects(), medicationDetailsDTO.getDosage());

    }


}
