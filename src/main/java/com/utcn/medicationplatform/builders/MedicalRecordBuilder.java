package com.utcn.medicationplatform.builders;

import com.utcn.medicationplatform.dtos.MedicalRecordDetailsDTO;
import com.utcn.medicationplatform.model.MedicalRecord;
import com.utcn.medicationplatform.model.Patient;

public class MedicalRecordBuilder {

    public MedicalRecordBuilder() {
    }

    public static MedicalRecord toEntity(MedicalRecordDetailsDTO medicalRecordDetailsDTO){
        return new MedicalRecord(medicalRecordDetailsDTO.getStart_date(),
                medicalRecordDetailsDTO.getEnd_date(),
                medicalRecordDetailsDTO.getMedicalCondition(),
                medicalRecordDetailsDTO.getIntake_interval()
                );
    }

}
