package com.utcn.medicationplatform.builders;

import com.utcn.medicationplatform.dtos.ActivityDetailsDTO;
import com.utcn.medicationplatform.model.Activity;

public class ActivityBuilder {


    private ActivityBuilder(){
    }

    public static Activity toEntity(ActivityDetailsDTO activityDetailsDTO){
        return new Activity(activityDetailsDTO.getPatient_id(),
                activityDetailsDTO.getActivity(),
                activityDetailsDTO.getStart(),
                activityDetailsDTO.getEnd()
        );
    }


}
