package com.utcn.medicationplatform.builders;

import com.utcn.medicationplatform.dtos.DoctorDTO;
import com.utcn.medicationplatform.dtos.DoctorDetailsDTO;
import com.utcn.medicationplatform.model.Doctor;
import com.utcn.medicationplatform.model.User;

public class DoctorBuilder {

    private DoctorBuilder(){

    }

    public static DoctorDTO toDoctorDTO(Doctor doctor){

        return new DoctorDTO(doctor.getName(),
                doctor.getAdress(),
                doctor.getDate(),
                doctor.getUser().getUsername(),
                doctor.getUser().getPassword());
    }

    public static Doctor toEntity(DoctorDetailsDTO doctorDetailsDTO){
        return new Doctor(doctorDetailsDTO.getName(),
                doctorDetailsDTO.getBirthdate(),
                doctorDetailsDTO.getAdress(),
                doctorDetailsDTO.getUser());

    }

    public static User toEntityUser(DoctorDetailsDTO doctorDetailsDTO){
        return new User(doctorDetailsDTO.getUsername(),
                doctorDetailsDTO.getPassword(),
                doctorDetailsDTO.getType());
    }


}
