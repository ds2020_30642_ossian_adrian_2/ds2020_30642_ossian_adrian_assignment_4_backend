package com.utcn.medicationplatform.builders;

import com.utcn.medicationplatform.dtos.PatientDTO;
import com.utcn.medicationplatform.dtos.PatientDetailsDTO;
import com.utcn.medicationplatform.model.Patient;
import com.utcn.medicationplatform.model.User;

public class PatientBuilder {

    private PatientBuilder(){
    }

    public static PatientDTO toPatientDTO(Patient patient){

        return new PatientDTO(patient.getName(),
                patient.getAdress(),
                patient.getGender(),
                patient.getBirthdate(),
                patient.getUser().getUsername(),
                patient.getUser().getPassword()
        );
    }

    public static PatientDTO toPatientDTOForPresentingToCaregiver(Patient patient){

        return new PatientDTO(patient.getName(),
                patient.getAdress(),
                patient.getGender(),
                patient.getBirthdate()
        );
    }

    public static Patient toEntity(PatientDetailsDTO patientDetailsDTO){
        return new Patient(patientDetailsDTO.getName(),
                patientDetailsDTO.getBirthdate(),
                patientDetailsDTO.getGender(),
                patientDetailsDTO.getAdress(),
                patientDetailsDTO.getUser()
                );
    }

    public static User toEntityUser(PatientDetailsDTO patientDetailsDTO){
        return new User(patientDetailsDTO.getUsername(),
                patientDetailsDTO.getPassword(),
                patientDetailsDTO.getType()
                );


    }

}
