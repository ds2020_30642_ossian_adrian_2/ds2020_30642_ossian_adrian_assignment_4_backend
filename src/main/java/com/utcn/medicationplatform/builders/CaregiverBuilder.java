package com.utcn.medicationplatform.builders;

import com.utcn.medicationplatform.dtos.CaregiverDTO;
import com.utcn.medicationplatform.dtos.CaregiverDetailsDTO;
import com.utcn.medicationplatform.model.Caregiver;
import com.utcn.medicationplatform.model.Patient;
import com.utcn.medicationplatform.model.User;

public class CaregiverBuilder {

    private CaregiverBuilder(){
    }

    public static CaregiverDTO toCaregiverDTO(Caregiver caregiver){

        return new CaregiverDTO(caregiver.getName(),
                caregiver.getAdress(),
                caregiver.getGender(),
                caregiver.getDate(),
                caregiver.getUser().getUsername(),
                caregiver.getUser().getPassword()
        );
    }

    public static Caregiver toEntity(CaregiverDetailsDTO CaregiverDTO){
        return new Caregiver(CaregiverDTO.getName(),
                CaregiverDTO.getBirthdate(),
                CaregiverDTO.getGender(),
                CaregiverDTO.getAdress(),
                CaregiverDTO.getUser()
        );
    }

    public static User toEntityUser(CaregiverDetailsDTO caregiverDetailsDTO){
        return new User(caregiverDetailsDTO.getUsername(),
                caregiverDetailsDTO.getPassword(),
                caregiverDetailsDTO.getType()
        );


    }

}
