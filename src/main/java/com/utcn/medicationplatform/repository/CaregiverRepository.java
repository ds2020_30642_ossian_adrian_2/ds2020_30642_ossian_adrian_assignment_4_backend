package com.utcn.medicationplatform.repository;

import com.utcn.medicationplatform.model.Caregiver;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface CaregiverRepository extends JpaRepository<Caregiver, UUID> {


    Optional<Caregiver> findByName(String name);

    Optional<Caregiver> findByUserId(UUID id);


}
