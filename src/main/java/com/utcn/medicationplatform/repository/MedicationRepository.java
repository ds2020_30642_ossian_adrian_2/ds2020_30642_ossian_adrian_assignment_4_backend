package com.utcn.medicationplatform.repository;

import com.utcn.medicationplatform.model.Medication;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface MedicationRepository extends JpaRepository<Medication, UUID> {

    int deleteByName(String name);
    Optional<Medication> findByName(String name);


}
