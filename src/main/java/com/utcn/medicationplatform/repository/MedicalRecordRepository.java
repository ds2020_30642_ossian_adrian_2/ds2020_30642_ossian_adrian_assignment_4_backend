package com.utcn.medicationplatform.repository;

import com.utcn.medicationplatform.model.MedicalRecord;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface MedicalRecordRepository extends JpaRepository<MedicalRecord, UUID> {


    Optional<MedicalRecord> findByPatientName(String name);

}
