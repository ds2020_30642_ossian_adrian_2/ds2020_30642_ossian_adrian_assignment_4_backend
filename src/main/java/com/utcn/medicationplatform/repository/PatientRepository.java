package com.utcn.medicationplatform.repository;

import com.utcn.medicationplatform.model.Patient;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface PatientRepository extends JpaRepository<Patient, UUID> {

    List<Patient> findByCaregiverId(UUID id);
    Optional<Patient> findByName(String name);
    Optional<Patient> findByUserId(UUID id);

}
