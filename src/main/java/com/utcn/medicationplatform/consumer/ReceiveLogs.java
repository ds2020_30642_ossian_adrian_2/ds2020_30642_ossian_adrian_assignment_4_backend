package com.utcn.medicationplatform.consumer;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DeliverCallback;
import com.utcn.medicationplatform.dtos.ActivityDetailsDTO;
import com.utcn.medicationplatform.dtos.PatientDTO;
import com.utcn.medicationplatform.model.ReceivedActivity;
import com.utcn.medicationplatform.notification.AnomalyMessage;
import com.utcn.medicationplatform.service.ActivityService;
import com.utcn.medicationplatform.service.PatientService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.util.UUID;

@Slf4j
@CrossOrigin
@Component
public class ReceiveLogs {
    private static final String EXCHANGE_NAME = "logs";

    public static ActivityService activityService;

    public static SimpMessagingTemplate template;

    public static PatientService patientService;

    static int numberOfNotifications = 0;



    @Autowired
    public ReceiveLogs(ActivityService activityService, SimpMessagingTemplate simpMessagingTemplate, PatientService patientService) {
        this.activityService = activityService;
        this.template = simpMessagingTemplate;
        this.patientService = patientService;
    }

    public static void receiveMessages() throws Exception {


        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("jaguar-01.rmq.cloudamqp.com");
        factory.setVirtualHost("qpgmkslh");
        factory.setPassword("UXkno39q6jQ8vW1-xF1T4iaxgKXqFSFy");
        factory.setUsername("qpgmkslh");
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();

        channel.exchangeDeclare(EXCHANGE_NAME, "fanout");
        String queueName = channel.queueDeclare().getQueue();
        channel.queueBind(queueName, EXCHANGE_NAME, "");


        log.info("ReceiveLogs started. Waiting for messages");

        DeliverCallback deliverCallback = (consumerTag, delivery) -> {

            //read activity object
            ByteArrayInputStream bis = new ByteArrayInputStream(delivery.getBody());
            ObjectInput in = null;
            try {
                in = new ObjectInputStream(bis);
                ReceivedActivity currentActivity = (ReceivedActivity)in.readObject();

                log.info(" [x] Received '" + currentActivity + "'");

                Long spentTime = currentActivity.getEnd() - currentActivity.getStart();

                ActivityDetailsDTO activityDetailsDTO = new ActivityDetailsDTO(currentActivity.getPatient_id(), currentActivity.getActivity(), currentActivity.getStart(), currentActivity.getEnd());
                PatientDTO patientDTO = patientService.findPatientById(currentActivity.getPatient_id());

                //analize activities for anomalies
                switch(currentActivity.getActivity())
                {
                    case "Sleeping":
                        if(spentTime >= 25200000){ //7 hours
                            numberOfNotifications++;
                            UUID id = activityService.insert(activityDetailsDTO);
                            log.info("[Anomaly] For the activity: "+ currentActivity);
                            template.convertAndSend("/topic/greetings", new AnomalyMessage("[ " + numberOfNotifications + " ] "+ "The patient " + patientDTO.getName() + " might have some problems regarding the activity " + currentActivity.getActivity()));

                        }
                        break;
                    case "Leaving":
                        if(spentTime >= 18000000){ //5 hours
                            numberOfNotifications++;
                            UUID id = activityService.insert(activityDetailsDTO);
                            log.info("[Anomaly] For the activity: "+ currentActivity);
                            template.convertAndSend("/topic/greetings", new AnomalyMessage("[ " + numberOfNotifications + " ] "+ "The patient " + currentActivity.getPatient_id().toString() + " might have some problems regarding the activity " + currentActivity.getActivity()));

                        }
                        break;
                    case "Toileting":
                        if(spentTime >= 1800000){ //30 min
                            numberOfNotifications++;
                            UUID id = activityService.insert(activityDetailsDTO);
                            log.info("[Anomaly] For the activity: "+ currentActivity);
                            template.convertAndSend("/topic/greetings", new AnomalyMessage("[ " + numberOfNotifications + " ] "+ "The patient " + currentActivity.getPatient_id().toString() + " might have some problems regarding the activity " + currentActivity.getActivity()));

                        }
                        break;
                    default:
                        System.out.println("");
                }

            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            } finally {
                try {
                    if (in != null) {
                        in.close();
                    }
                } catch (IOException ex) {
                    // ignore close exception
                }
            }


        };
        channel.basicConsume(queueName, true, deliverCallback, consumerTag -> { });
    }
}