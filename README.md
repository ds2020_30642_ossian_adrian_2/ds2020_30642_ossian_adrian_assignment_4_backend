# Medical Services Application

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)


# Build considerations

All you need is the IntelliJ Idea IDE and Java 11. Clone the project to your local machine and open the pom.xml file as a project.
Let the IDE handle the installing of the required dependencies. 

# Execution considerations
Open application.properties file and:
 - Create a database in postgreSQL named MedicalServicesFinal
 - Change DB_USER and DB_PASSWORD according to your computer's database credentials
 - Change spring.jpa.hibernate.ddl-auto variable from _validate_ to _create_
 - Run the project
 - Change back from _create_ to _validate_ and re-run
 - Create a Database Admin (a doctor) from Postman using the following JSON and the following API as an example
 
 JSON:
{
    "name": "Doctor DRE",
    "birthdate": "2000-02-02",
    "adress": "Somewhere in the streets",
    "username": "Doctor_DRE",
    "password": "secure_password"
 } 
API:
http://localhost:8080/doctor
METHOD: 
post



# New Features 

  - Insert a medical record of the patient
  - Updated UI controlls
